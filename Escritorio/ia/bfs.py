#from graphviz import Digraph

H = {'A': set(['B', 'C']),
	'B': set(['A', 'D', 'E']),
    'C': set(['A', 'F']),
    'D': set(['B']),
    'E': set(['B', 'F']),
    'F': set(['C', 'E'])}

def bfs(G,s,Goal):
	found=False
	fringe=[s]
	level={s:0}
	i=1
	while fringe and not found:
		children=[]
		for u in fringe:
			for v in G[u]:
				if v not in level and not found:
					level[v]=i
					children.append(v)
					if v== Goal:
						found=True
						print "Encontrado"
			fringe=children
			i=i+1
	for elm in level:
		print elm
"""		
digraph tree {
"A"->"(B)";
"A"->"(C)";
"(B)"->"(D)";
"(B)"->"(E)";
"(C)"->"(F)";
"(D)"->"(B)";
"(E)"->"(B)";
"(E)"->"(F)";
"(F)"->"(C)";
"(F)"->"(E)";

}"""

bfs(H,'A','E')
